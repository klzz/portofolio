<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Contact;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
class klaasController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function createAction(Request $request)
    {
        $contact = new Contact;

        $form = $this->createFormBuilder($contact)
            ->add('email', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('name', TextType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('message', TextareaType::class, array('attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('submit', SubmitType::class, array('label' => 'Send!', 'attr' => array('class' => 'btn btn-primary', 'style' => 'margin-bottom:15px')))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $email = $form['email']->getData();
            $name = $form['name']->getData();
            $message = $form['message']->getData();

            $contact->setEmail($email);
            $contact->setName($name);
            $contact->setMessage($message);

            $em = $this->getDoctrine()->getManager();

            $em->persist($contact);
            $em->flush();

        }

        return $this->render('default/index.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/contact", name="contact_list")
     */
    public function listAction(Request $request)
    {
        $messages = $this->getDoctrine()
            ->getRepository('AppBundle:Contact')
            ->findAll();
        return $this->render('contact/contact.html.twig', array(
            'messages' => $messages
        ));
    }

    /**
     * @Route("contact/delete/{id}", name="message_delete")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
            $contact = $em->getRepository('AppBundle:Contact')->find($id);

            $em->remove($contact);
            $em->flush();

            return $this->redirectToRoute('contact_list');
    }
}